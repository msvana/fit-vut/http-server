#!/usr/bin/env python3
import unittest
from responsebody import make_response_body
from responsebody import DoesNotExist
from responsebody import Forbidden


class TestResponseBodyMaking(unittest.TestCase):
	
	def test_response_does_not_exist(self):
		request = self.make_request_to_path("/unknown_file.txt")
		with self.assertRaises(DoesNotExist): 
			response_body = make_response_body(request)

	def test_response_file(self):
		request = self.make_request_to_path("/dir/test.txt")
		response_body = make_response_body(request)
		self.assertEqual(response_body, "some content of file is here\n")

	def test_response_dir(self):
		request = self.make_request_to_path("/dir/")
		response_body = make_response_body(request)
		expected = "DIR\n- test2.txt\n- test.txt\n"
		self.assertEqual(expected, response_body)

	def make_request_to_path(self, path):
		request = dict(method="GET", uri=path, path=path, http_method="HTTP/1.1")
		return request
