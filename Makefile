.PHONY: clean

test:
	./test.sh

clean:
	@echo "Removing .log, .header, .payload and .tgz files"
	@rm -f *.log
	@rm -f *.header
	@rm -f *.payload
	@rm -f *.tgz

pack: clean
	@echo "Creating package for WIS"
	@tar -czf xsvana01.tgz WWW src Makefile ipkhttpclient ipkhttpserver README.md test.sh
