#!/usr/bin/env python3
import os
from config import BASE_PATH


class DoesNotExist(Exception):
	"""
	Thrown when requested file or directory is not found in BASE_PATH folder
	Should result into returning error 404 to the client
	"""


class Forbidden(Exception):
	"""
	Thrown when http server user is not allowed to read the file or directory
	Should result into returning error 403 to the client
	"""


def make_response_body(request):
	"""
	Creates body of a response (need to connect headers) which will be sent
	to client. Response body is created differently depending on whether 
	requested path refers to a directory or a file

	Param request: dictionary returned by request.make_request_from_text()
	Returns: string containing response body

	Throws: DoesNotExist, Forbidden
	"""
	path = "%s%s" %(BASE_PATH, request["path"])
	
	if not os.path.exists(path):
		message = "Could not find %s file or directory" % request["path"]
		raise DoesNotExist(message)
	
	if os.path.isdir(path):
		return make_response_body_directory(path)

	if os.path.isfile(path):
		return make_response_body_file(path)


def make_response_body_directory(path):
	"""
	If client requested path refers to a directory, list of it's files and
	subdirectories will be used as response body

	Param path: to requested directory
	Returns: list of directory items in string form
	
	Throws: Forbidden
	"""
	try:
		dir_contents = os.listdir(path)
	except PermissionError:
		message = "Cannot open %s directory for reading" % path
		raise Forbidden(message)

	contents_string = "DIR\n"
	for item in dir_contents:
		contents_string += "- %s\n" % item
	return contents_string

def make_response_body_file(path):
	"""
	If client requested path refers to a file, it's contents will be used
	as response body

	Param path: to requested file
	Returns: file contents

	Throws: Forbidden
	"""
	try:
		file_handler = open(path, "r")
	except PermissionError:
		message = "Cannot open %s file for reading" % path
		raise Forbidden(message)

	contents = file_handler.read()
	file_handler.close()
	return contents
